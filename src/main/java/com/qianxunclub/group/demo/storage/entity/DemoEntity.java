package com.qianxunclub.group.demo.storage.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.qianxunclub.mysql.base.model.BaseEntity;
import lombok.Data;

/**
 * @author chihiro.zhang
 */
@Data
@TableName("demo")
public class DemoEntity extends BaseEntity {
    @TableId(value="id",type=IdType.AUTO)
    private Integer id;
    private String appId;
}
