package com.qianxunclub.group.demo.storage.dao;


import com.qianxunclub.group.demo.model.request.DemoParam;
import com.qianxunclub.group.demo.storage.entity.DemoEntity;
import com.qianxunclub.group.demo.storage.mapper.DemoMapper;
import com.qianxunclub.mysql.base.dao.BaseDao;
import org.springframework.stereotype.Component;

/**
 * @author chihiro.zhang
 */
@Component
public class DemoDao extends BaseDao<DemoMapper, DemoEntity,DemoParam> {

}
