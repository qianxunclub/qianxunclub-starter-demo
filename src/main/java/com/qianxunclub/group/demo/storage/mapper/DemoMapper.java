package com.qianxunclub.group.demo.storage.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.qianxunclub.group.demo.storage.entity.DemoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author chihiro.zhang
 */
@Mapper
public interface DemoMapper extends BaseMapper<DemoEntity> {
}
