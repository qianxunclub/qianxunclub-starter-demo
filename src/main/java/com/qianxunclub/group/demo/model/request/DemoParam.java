package com.qianxunclub.group.demo.model.request;

import com.qianxunclub.mysql.base.model.BaseParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;

import javax.validation.constraints.NotNull;

/**
 * @author chihiro.zhang
 */
@Data
@ApiModel("查询入参")
public class DemoParam extends BaseParam {
    @ApiModelProperty(value="id")
    private String id;
    @ApiModelProperty(value="appId")
    @NotNull(groups = GetMapping.class)
    private String appId;
}
