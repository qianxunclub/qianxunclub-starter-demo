package com.qianxunclub.group.demo.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author chihiro.zhang
 */
@Data
@Component
@ConfigurationProperties(prefix = "constant")
public class ConfigProperties {

}
