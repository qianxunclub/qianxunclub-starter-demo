package com.qianxunclub.group.demo.web;

import com.qianxunclub.common.framework.response.Result;
import com.qianxunclub.group.demo.model.request.DemoParam;
import com.qianxunclub.group.demo.service.DemoService;
import com.qianxunclub.mysql.base.model.IdParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * DEMO接口
 * @author chihiro.zhang
 */
@Slf4j
@RestController
@RequestMapping(value = "/demo")
@AllArgsConstructor
@Api(description = "DEMO接口")
public class DemoController {

	private DemoService demoService;

	@ApiOperation("列表")
	@GetMapping(value = "list")
	public Result list(@Validated(value = GetMapping.class) DemoParam param) {
		Result result = demoService.list(param);
		return result;
	}

	@ApiOperation("查询一条")
	@GetMapping(value = "query")
	public Result query(@Validated IdParam param) {
		Result result = demoService.queryById(param.getId());
		return result;
	}

}
