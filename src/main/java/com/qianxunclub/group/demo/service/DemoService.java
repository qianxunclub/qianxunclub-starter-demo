package com.qianxunclub.group.demo.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.qianxunclub.common.framework.response.Result;
import com.qianxunclub.group.demo.model.request.DemoParam;
import com.qianxunclub.group.demo.storage.dao.DemoDao;
import com.qianxunclub.group.demo.storage.entity.DemoEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author chihiro.zhang
 */
@Slf4j
@Component
@AllArgsConstructor
public class DemoService {

    private DemoDao demoDao;

    public Result list(DemoParam param){

        EntityWrapper<DemoEntity> ew = new EntityWrapper();
        Page<DemoEntity> demoPage =  demoDao.list(ew,param);
        return Result.result(demoPage);
    }

    public Result queryById(Integer id){
        DemoEntity entity = demoDao.selectById(id);
        return Result.result(entity);
    }

}
